#define FUSE_USE_VERSION 31

#include <stdio.h>
#include <fuse.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define UNUSED(x) (void)(x)

static const char *info_str = "T File System, version 0.0.1";
static const char *test_mount_point = "/tmp/tfs_test";


static int tfs_getattr(const char *path, struct stat *st) {
    printf("[getattr] %s\n", path);
    // the id here is from whoever run the program
    uid_t uid = getuid();
    gid_t gid = getgid();
    st->st_uid = uid;
    st->st_gid = gid;
    // access time
    st->st_atime = time(NULL);
    // modified time
    st->st_mtime = time(NULL);

    if (!strcmp(path, "/")) {
        // directory with 777
        st->st_mode = S_IFDIR | 0555;
        // 2 hard links, . and ..
        st->st_nlink = 2;
    } else if (!strcmp(path, "/tfs_info.txt")) {
        // regular file
        st->st_mode = S_IFREG | 0444;
        // 1 hard link to its own data
        st->st_nlink = 1;
        // size 256
        st->st_size = strlen(info_str);
    }
    return 0;
}


static int tfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t _offset,
                       struct fuse_file_info *_file_info) {
    UNUSED(_offset);
    UNUSED(_file_info);
    printf("[readdir] %s\n", path);
    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);
    if (!strcmp(path, "/")) {
        filler(buf, "tfs_info.txt", NULL, 0);
    }
    return 0;
}


static int tfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *_file_info) {
    UNUSED(_file_info);
    printf("[read] (offset: %lld) (size: %ld) %s\n", offset, size, path);
    if (!strcmp(path, "/tfs_info.txt")) {
        int infolen = strlen(info_str);
        if (offset > infolen) {
            return 0;
        }
        strncpy(buf, info_str + offset, size);
        return strlen(info_str + offset);
    }
    return -1;
}


static struct fuse_operations fuse_ops = {
    .getattr = tfs_getattr,
    .readdir = tfs_readdir,
    .read = tfs_read,
};


static int check_for_test(int argc, char **argv) {
    if (argc == 2) {
        if (!strcmp(argv[1], "--test")) {
            struct stat st = {0};
            if (stat(test_mount_point, &st) == -1) {
                mkdir(test_mount_point, 0777);
            }
            char *new_argv[3] = {argv[0], "-f", (char *) test_mount_point};
            fuse_main(3, new_argv, &fuse_ops, NULL);
            return 1;
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    printf("Launching TFS\n");
    if (!check_for_test(argc, argv)) return 0;
    return fuse_main(argc, argv, &fuse_ops, NULL);
}
